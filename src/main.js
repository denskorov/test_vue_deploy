import { createApp } from "vue";
import { createWebHistory, createRouter } from "vue-router";
import App from "./App.vue";
import Index from "./Index.vue";
import History from "./History.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: "/",
      component: Index,
      name: "home",
    },
    {
      path: "/history",
      component: History,
      name: "history",
    },
  ],
});

createApp(App).use(router).mount("#app");
